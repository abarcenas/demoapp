# Base image:
FROM ruby:2.5.0

# Install dependencies
RUN apt-get update -yqq \
 && apt-get install -y build-essential \
    libpq-dev \
    nodejs \
    netcat \
 && apt-get -q clean \
 && rm -rf /var/lib/apt/lists


# Set an environment variable where the Rails app is installed to inside of Docker image:
WORKDIR /usr/src/app
COPY Gemfile* ./
RUN bundle install
COPY . .

CMD puma -C config/puma.rb -e development --debug -v
