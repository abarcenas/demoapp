# DEMOAPP 
This is a very simple Ruby on Rails app.

# Ruby version
2.5.0 (.ruby-version) 
# Rails Version
5.2.0

# System dependencies
* Ruby 2.5.0
* Mysql2-client (.rpm or .deb) for mysql2 gem
* Mysql Database

# Database Configuration
For both Docker and standalone mysql, you must define the environment variable DATABASE_URL="mysql2/user:pass@host:/db_name" for the app to work.

# How to run the app
## Docker
Simple run docker-compose up -d and access the app via http://HOST:3000. The database must be initialized before using the app.


# Database Creation and Initialization
## Docker

Make sure DATABASE_URL env variables is properly defined in docker-compose.yml, then run the following commands to initialize the db

`docker-compose run --rm demoapp rake db:setup`
`docker-compose run --rm demoapp rake db:migrate`

## Standalone app
rails rake db:setup
rails rake db:migrate

# Deployment instructions
To deploy the app to AWS, you can simple run a playbook inside the ansible directory
`cd ansible && ansible-playbook demoapps_deploy.yml`

This playbook will spin up 1 ec2 instance and install all system requirements for Ruby on Rails

*Make sure you have AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY environment variables defined*

