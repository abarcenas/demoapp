Rails.application.routes.draw do
  resources :personal_preferences
  root 'personal_preferences#index'
end
