class CreatePersonalPreferences < ActiveRecord::Migration[5.2]
  def change
    create_table :personal_preferences do |t|
      t.string :name, unique: true
      t.string :favorite_color
      t.integer :favorite_animal

      t.timestamps
    end
  end
end
