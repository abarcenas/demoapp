class PersonalPreference < ApplicationRecord
  enum favorite_animal: { cats: 0, dogs: 1}
  validates :name, uniqueness: true, presence: true
  validates :favorite_color, presence: true
end