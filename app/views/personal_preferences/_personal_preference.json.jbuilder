json.extract! personal_preference, :id, :name, :favorite_color, :favorite_animal, :created_at, :updated_at
json.url personal_preference_url(personal_preference, format: :json)
