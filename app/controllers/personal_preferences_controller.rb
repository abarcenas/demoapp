class PersonalPreferencesController < ApplicationController
  before_action :set_personal_preference, only: [:show, :edit, :update, :destroy]

  # GET /personal_preferences
  # GET /personal_preferences.json
  def index
    @personal_preferences = PersonalPreference.all
  end

  # GET /personal_preferences/1
  # GET /personal_preferences/1.json
  def show
  end

  # GET /personal_preferences/new
  def new
    @personal_preference = PersonalPreference.new
  end

  # GET /personal_preferences/1/edit
  def edit
  end

  # POST /personal_preferences
  # POST /personal_preferences.json
  def create
    @personal_preference = PersonalPreference.new(personal_preference_params)

    respond_to do |format|
      if @personal_preference.save
        format.html { redirect_to @personal_preference, notice: 'Personal preference was successfully created.' }
        format.json { render :show, status: :created, location: @personal_preference }
      else
        format.html { render :new }
        format.json { render json: @personal_preference.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /personal_preferences/1
  # PATCH/PUT /personal_preferences/1.json
  def update
    respond_to do |format|
      if @personal_preference.update(personal_preference_params)
        format.html { redirect_to @personal_preference, notice: 'Personal preference was successfully updated.' }
        format.json { render :show, status: :ok, location: @personal_preference }
      else
        format.html { render :edit }
        format.json { render json: @personal_preference.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /personal_preferences/1
  # DELETE /personal_preferences/1.json
  def destroy
    @personal_preference.destroy
    respond_to do |format|
      format.html { redirect_to personal_preferences_url, notice: 'Personal preference was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_personal_preference
      @personal_preference = PersonalPreference.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def personal_preference_params
      params.require(:personal_preference).permit(:name, :favorite_color, :favorite_animal)
    end
end
